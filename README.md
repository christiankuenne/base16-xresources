# base16-xresources

This repository is meant to work with [chriskempson/base16](https://github.com/chriskempson/base16).
It provides a simple template that can be used with the base16 color schemes to generate a config file which can be referenced from .Xresources.